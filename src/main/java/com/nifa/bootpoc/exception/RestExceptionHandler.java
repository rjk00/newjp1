package com.nifa.bootpoc.exception;

import javax.validation.ConstraintViolationException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * Handle EntityNotFoundException.  Thrown when resource cannot be found.
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(EntityNotFoundException.class)
	ResponseEntity<Object> handleEntityNotFound(
            EntityNotFoundException ex) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        return buildResponseEntity(apiError);
    }
	
    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
        apiError.setMessage("Validation error");
        apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
        apiError.addValidationError(ex.getBindingResult().getGlobalErrors());
        return buildResponseEntity(apiError);
    }
    
    /**
     * Handles ConstraintViolationException. Thrown when @Validated fails.
     *
     * @param ex the ConstraintViolationException
     * @return the ResponseEntity object wrapped around ApiError
     */
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
        apiError.setMessage("Validation error");
        apiError.addValidationErrors(ex.getConstraintViolations());
        return buildResponseEntity(apiError);
    }
    
    /**
     * Handles UnrecognizedPropertyException.
     * Thrown when client passes in a JSON property that Jackson cannot parse.
     * 
     * @param ex the UnrecognizedPropertyException
     * @return the ResponseEntity object wrapped around ApiError
     */
    @ExceptionHandler(UnrecognizedPropertyException.class)
    protected ResponseEntity<Object> handleUnrecognizedProperty(UnrecognizedPropertyException ex) {
    	ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
    	apiError.setMessage("Invalid property error");
    	apiError.addJacksonErrors(ex.getPath());
    	return buildResponseEntity(apiError);
    }
    
    /**
     * Handles JsonParseException.
     * Thrown when client passes in JSON data with invalid syntax. 
     * 
     * @param ex the JsonParseException
     * @return the ResponseEntity object wrapped around ApiError
     */
    @ExceptionHandler(JsonParseException.class)
    protected ResponseEntity<Object> handleJsonParseException(JsonParseException ex) {
    	ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
    	apiError.setMessage("Invalid JSON syntax");
    	return buildResponseEntity(apiError);
    }

    @Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
    	
    	Throwable cause = ex.getCause();
    	
        if (cause instanceof UnrecognizedPropertyException) {
        	return handleUnrecognizedProperty((UnrecognizedPropertyException) cause);
        }else if (cause instanceof JsonParseException) {
        	return handleJsonParseException((JsonParseException) cause);
        }
        
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
		apiError.setMessage("Required body is missing");
		return buildResponseEntity(apiError);
	}

	private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
