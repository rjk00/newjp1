package com.nifa.bootpoc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "critical_issue")
public class CriticalIssue {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plan_of_work_id")
	private PlanOfWork plan;
	
	@Column(nullable = false)
	private Integer issueOrder;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Term term;
	
    private enum Term
    { 
        SHORT("Short-Term: Under 1 year"),
        INTERMEDIATE("Intermediate: 1 to 5 years"),
        LONG("Long-Term: 5 or more years");
    	
        private final String description;
        
    	Term(String description) {
    		this.description = description;
    	}
    	
    	public String getDescription() {
    		return description;
    	}
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlanOfWork getPlan() {
		return plan;
	}

	public void setPlan(PlanOfWork plan) {
		this.plan = plan;
	}

	public Integer getOrder() {
		return issueOrder;
	}

	public void setOrder(Integer order) {
		this.issueOrder = order;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public String getTermDescription() {
    	return term.getDescription();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CriticalIssue other = (CriticalIssue) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CriticalIssue [id=" + id + ", plan=" + plan + ", order=" + issueOrder + ", name=" + name + ", term=" + term
				+ "]";
	}
}
