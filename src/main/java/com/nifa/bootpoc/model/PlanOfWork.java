package com.nifa.bootpoc.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "plan_of_work")
public class PlanOfWork implements Comparable<PlanOfWork>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Integer year;
	
	@Column(name = "status", nullable = false)
	private String planStatus;

	@Column(nullable = false)
	private String cohort;
	
	@Column(name = "executive_summary", length = 1024)
	private String executiveSummary;

	@Column(name = "merit_peer_review", length = 1024)
	private String meritPeerReview;
	
	@Column(name = "stakeholder_actions", length = 1024)
	private String stakeHolderActions;
	
	@Column(name = "stakeholder_id_methods", length = 1024)
	private String stakeHolderIdMethods;
	
	@Column(name = "stakeholder_collection_method", length = 1024)
	private String stakeHolderCollectionMethod;
	
	@Column(name = "stakeholder_how_considered", length = 1024)
	private String stakeHolderHowConsidered;
	
	@Column(name = "critical_issues")
	@OneToMany(
			mappedBy = "plan",
			cascade = CascadeType.ALL,
			orphanRemoval = true
	)
	private Set<CriticalIssue> criticalIssues;
	
	
	public PlanOfWork() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}


	public String getPlanStatus() {
		return planStatus;
	}


	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}


	public String getCohort() {
		return cohort;
	}


	public void setCohort(String cohort) {
		this.cohort = cohort;
	}


	public String getExecutiveSummary() {
		return executiveSummary;
	}


	public void setExecutiveSummary(String executiveSummary) {
		this.executiveSummary = executiveSummary;
	}


	public String getMeritPeerReview() {
		return meritPeerReview;
	}


	public void setMeritPeerReview(String meritPeerReview) {
		this.meritPeerReview = meritPeerReview;
	}


	public String getStakeHolderActions() {
		return stakeHolderActions;
	}


	public void setStakeHolderActions(String stakeHolderActions) {
		this.stakeHolderActions = stakeHolderActions;
	}


	public String getStakeHolderIdMethods() {
		return stakeHolderIdMethods;
	}


	public void setStakeHolderIdMethods(String stakeHolderIdMethods) {
		this.stakeHolderIdMethods = stakeHolderIdMethods;
	}


	public String getStakeHolderCollectionMethod() {
		return stakeHolderCollectionMethod;
	}


	public void setStakeHolderCollectionMethod(String stakeHolderCollectionMethod) {
		this.stakeHolderCollectionMethod = stakeHolderCollectionMethod;
	}


	public String getStakeHolderHowConsidered() {
		return stakeHolderHowConsidered;
	}


	public void setStakeHolderHowConsidered(String stakeHolderHowConsidered) {
		this.stakeHolderHowConsidered = stakeHolderHowConsidered;
	}


	public Set<CriticalIssue> getCriticalIssues() {
		return criticalIssues;
	}


	public void setCriticalIssues(Set<CriticalIssue> criticalIssues) {
		this.criticalIssues = criticalIssues;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanOfWork other = (PlanOfWork) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "PlanOfWork [id=" + id + ", year=" + year + ", planStatus=" + planStatus + ", cohort=" + cohort
				+ ", executiveSummary=" + executiveSummary + ", meritPeerReview=" + meritPeerReview
				+ ", stakeHolderActions=" + stakeHolderActions + ", stakeHolderIdMethods=" + stakeHolderIdMethods
				+ ", stakeHolderCollectionMethod=" + stakeHolderCollectionMethod + ", stakeHolderHowConsidered="
				+ stakeHolderHowConsidered + ", criticalIssues=" + criticalIssues + "]";
	}


	@Override
	public int compareTo(PlanOfWork o) {
		return (int)(id - o.id);
	}
}