package com.nifa.bootpoc.service;

import java.util.Set;

import com.nifa.bootpoc.model.PlanOfWork;

public interface PlanOfWorkService {

	Long createPlan(PlanOfWork plan);
	Set<PlanOfWork> getPlans();
	PlanOfWork getPlanById(Long id);
	void updatePlan(PlanOfWork plan);
	void deletePlan(Long id);
}